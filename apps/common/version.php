<?php
return array(
    // 应用版本
    'app_version' => '2.1.0',
    
    // 发布时间
    'release_time' => '20200621',
    
    // 修订版本
    'revise_version' => '1'

);
